output "internal_ip_address_vm_infra_1" {
  value = yandex_compute_instance.vm-infra-1.network_interface.0.ip_address
}

output "external_ip_address_vm_infra_1" {
  value = yandex_compute_instance.vm-infra-1.network_interface.0.nat_ip_address
}

output "lb_infra_ip_address" {
  value = tolist(tolist(yandex_lb_network_load_balancer.nw-lb-infra-1.listener)[0].external_address_spec)[0].address
}
