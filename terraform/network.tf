resource "yandex_lb_target_group" "lb-tg-infra-1" {
  name      = "tg-infra-1"
  region_id = "ru-central1"

  target {
    subnet_id = "${yandex_vpc_subnet.sn-infra-1.id}"
    address   = "${yandex_compute_instance.vm-infra-1.network_interface.0.ip_address}"
  }
}

resource "yandex_lb_network_load_balancer" "nw-lb-infra-1" {
  name = "lb-infra-1"

  listener {
    name = "listener-1"
    port = 80
    external_address_spec {
        ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = "${yandex_lb_target_group.lb-tg-infra-1.id}"

    healthcheck {
      name = "http"
      interval = 10
      http_options {
        port = 80
      }
    }
  }
}

resource "yandex_vpc_network" "nw-infra-1" {
  name = "nw-infra-1"
}

resource "yandex_vpc_subnet" "sn-infra-1" {
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.nw-infra-1.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}

resource "yandex_dns_zone" "zone-infra-1" {
  name        = "public-zone-infra-1"
  description = "Public zone for final project"

  labels = {
    label1 = "m4n.fun"
  }

  zone             = "m4n.fun."
  public           = true
}

resource "yandex_dns_recordset" "rs-infra-1" {
  zone_id = yandex_dns_zone.zone-infra-1.id
  name    = "final-project.m4n.fun."
  type    = "A"
  ttl     = 200
  data = [[for value in yandex_lb_network_load_balancer.nw-lb-infra-1.listener : value.external_address_spec.*][0][0].address]
}
